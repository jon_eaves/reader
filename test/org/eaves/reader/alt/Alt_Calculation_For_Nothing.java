package org.eaves.reader.alt;

import org.eaves.reader.Calculation;

public class Alt_Calculation_For_Nothing implements Calculation {
    @Override
    public int calculate(int[] inputs) {
        return 0;
    }
}
