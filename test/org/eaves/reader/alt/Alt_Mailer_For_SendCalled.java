package org.eaves.reader.alt;

import org.eaves.reader.Mailer;

public class Alt_Mailer_For_SendCalled implements Mailer {
    private int _called;

    @Override
    public void send(String target, int value) {
        ++_called;
    }
    
    public int called() {
        return _called;
    }
}
