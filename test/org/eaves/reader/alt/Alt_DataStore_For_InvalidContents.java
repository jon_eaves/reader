package org.eaves.reader.alt;

import org.eaves.reader.DataStore;

public class Alt_DataStore_For_InvalidContents implements DataStore {

    @Override
    public int[] read() throws Exception {
        throw new Exception("No Data For You");
    }
}
