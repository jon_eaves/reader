package org.eaves.reader.alt;

import org.eaves.reader.Logger;

public class Alt_Logger_For_LogCalled implements Logger {

    private int _called = 0;
    
    @Override
    public void log(String message) {
        ++_called;
    }
    
    public int called() {
        return _called;
    }
}
