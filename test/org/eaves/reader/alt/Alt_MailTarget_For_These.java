package org.eaves.reader.alt;

import org.eaves.reader.MailTarget;

public class Alt_MailTarget_For_These implements MailTarget {
    
    private String[] _targets;
    
    public Alt_MailTarget_For_These(String[] targets) {
        _targets = targets;
    }

    @Override
    public String[] findTargets(int result) {
        return _targets;
    }
}
