package org.eaves.reader;

import org.junit.Before;
import org.junit.Test;
import prod.Prod_CalculationEngine;
import prod.Prod_S3_DataStore;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;

public class Test_Prod_S3_DataStore {

    private Prod_S3_DataStore _dataStore;

    private static String BUCKET = "s3::eaves/";
    private static String STORE = "reader.store";
    
    @Before
    public void setUp() {
        _dataStore = new Prod_S3_DataStore(BUCKET+STORE);
    }

    @Test
    public void emptyResultsThrowException() {
        // AWSS3Client s3 = new AWSS3Client(BUCKET);
        // s3.copyTo(localemptyFile, STORE);
        // assertExceptionThrown(_dataStore.read());
    }

    @Test
    public void noFileThrowException() {
        // AWSS3Client s3 = new AWSS3Client(BUCKET);
        // s3.deleteIfExists(STORE);
        // assertExceptionThrown(_dataStore.read());
    }
    
    @Test
    public void noBucketThrowException() {
        // AWSS3Client s3 = new AWSS3Client("notexisting");
        // assertExceptionThrown(_dataStore.read());
    }
    
    @Test
    public void noReadPermissionsThrowException() {
        // do stuff
        // assertExceptionThrown(_dataStore.read());
    }

    @Test
    public void fileWithDataResultsOK() {
        // AWSS3Client s3 = new AWSS3Client(BUCKET);
        // s3.copyTo(localValidFile, STORE);
        // assertTrue(_dataStore.read().count() > 0);
    }
}
