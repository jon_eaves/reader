package org.eaves.reader;

import org.eaves.reader.alt.*;
import org.junit.Before;
import org.junit.Test;
import prod.Prod_CalculationEngine;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;

public class Test_ProdCalculationEngine {

    private Calculation _c;

    @Before
    public void setUp() {
        _c = new Prod_CalculationEngine();
    }

    @Test
    public void emptyResultsAreZero() {

        assertEquals(0, _c.calculate(new int[] {}));
    }

    @Test
    public void singleResultIsSingleValue() {
        int x = 5;
        
        assertEquals( x, _c.calculate(new int[] { x }));
    }
    
    @Test
    public void addHowDoesItWork() {
        // normally a large matrix with complicated edge cases testing these values
        int[] complicated = new int[] { 1,2,3 };
        int value = Arrays.stream(complicated).sum();
        
        assertEquals(value, _c.calculate(complicated));
    }
    
}
