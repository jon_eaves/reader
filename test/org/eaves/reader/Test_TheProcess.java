package org.eaves.reader;

import org.eaves.reader.alt.*;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class Test_TheProcess {
    
    // log message, and make sure nobody is mailed
    @Test
    public void emptyFileLogError() {

        Logger log = new Alt_Logger_For_LogCalled();
        DataStore d = new Alt_DataStore_For_InvalidContents();
        Calculation c = new Alt_Calculation_For_Nothing();
        MailTarget targets = new Alt_MailTarget_For_These(new String[] { "first", "second"});
        Mailer m = new Alt_Mailer_For_SendCalled();

        TheProcess p = new TheProcess(log, d, c, targets, m);

        p.run();

        assertEquals(1, ((Alt_Logger_For_LogCalled) log).called());
        assertEquals(0, ((Alt_Mailer_For_SendCalled)m).called());
    }

    // don't log a message, and call targets
    @Test
    public void goodFileNoErrors() {
        
        Logger log = new Alt_Logger_For_LogCalled();
        DataStore d = new Alt_DataStore_For_ValidContents();
        Calculation c = new Alt_Calculation_For_Nothing();
        
        String recipients[] = new String[] { "first", "second"};
        MailTarget targets = new Alt_MailTarget_For_These(recipients);
        Mailer m = new Alt_Mailer_For_SendCalled();

        TheProcess p = new TheProcess(log, d, c, targets, m);

        p.run();
        
        assertEquals(0, ((Alt_Logger_For_LogCalled) log).called());
        assertEquals(recipients.length, ((Alt_Mailer_For_SendCalled)m).called());
        
    }
    
    
    
}
