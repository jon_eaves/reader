package prod;

import org.eaves.reader.DataStore;

public class Prod_S3_DataStore implements DataStore {
    public Prod_S3_DataStore(String bucket) {
        // talk to bucket, find file
    }

    @Override
    public int[] read() throws Exception {
        // open file, read results, check if only numbers, lucky prod always works
        return new int[]{ 2, 4 };
    }
}
