package prod;

import org.eaves.reader.Logger;

public class Prod_Sysout_Logger implements Logger {

    public Prod_Sysout_Logger() {
        System.out.println("org.eaves.reader.Logger initialised");
    }

    @Override
    public void log(String message) {
        System.out.println(message);
    }
}
