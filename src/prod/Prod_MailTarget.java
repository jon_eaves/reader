package prod;

import org.eaves.reader.MailTarget;

import java.util.ArrayList;
import java.util.List;

public class Prod_MailTarget implements MailTarget {
    private final String _always;
    private final int _threshold;
    private final String _post;

    public Prod_MailTarget(String always, int threshold, String postThreshold) {
        _always = always;
        _threshold = threshold;
        _post = postThreshold;
    }

    @Override
    public String[] findTargets(int result) {
        List<String> targets = new ArrayList();
        targets.add(_always);
        if (result > _threshold) {
            targets.add(_post);
        }
        return targets.stream().toArray(String[]::new);
    }
}
