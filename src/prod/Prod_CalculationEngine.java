package prod;

import org.eaves.reader.Calculation;

import java.util.Arrays;

public class Prod_CalculationEngine implements Calculation {
    @Override
    public int calculate(int[] inputs) {
        return Arrays.stream(inputs).sum();
    }
}
