package org.eaves.reader;

import prod.*;

public class Main_Process {
    
    public void main() {
        Logger log = new Prod_Sysout_Logger();
        DataStore r = new Prod_S3_DataStore("s3:bucket_name");
        Calculation c = new Prod_CalculationEngine();
        MailTarget targets = new Prod_MailTarget("malthrin@jerks.com", 50, "pewsey@jerks.com");
        Mailer m = new Prod_SMTP_Mailer("mail.google.com");

        TheProcess p = new TheProcess(log, r, c, targets, m);
        
        p.run();
    }        
}
