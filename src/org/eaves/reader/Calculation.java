package org.eaves.reader;

public interface Calculation {
    
    int calculate(int[] inputs);
    
}
