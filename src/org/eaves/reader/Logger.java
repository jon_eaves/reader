package org.eaves.reader;

public interface Logger {
    
    void log(String message);
    
}
