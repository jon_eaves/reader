package org.eaves.reader;

public interface MailTarget {

    String[] findTargets(int result);

}
