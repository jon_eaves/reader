package org.eaves.reader;

public interface DataStore {
    
    int[] read() throws Exception;
    
}
