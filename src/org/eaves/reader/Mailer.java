package org.eaves.reader;

public interface Mailer {
    
    void send(String target, int value);
    
}
