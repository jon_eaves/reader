package org.eaves.reader;

import java.util.Arrays;

public class TheProcess {
    private final Logger _logger;
    private final DataStore _store;
    private final Calculation _calc;
    private final MailTarget _targets;
    private final Mailer _mailer;

    public TheProcess(Logger log, DataStore r, Calculation c, MailTarget targets, Mailer m) {
        _logger = log;
        _store = r;
        _calc = c;
        _targets = targets;
        _mailer = m;
    }

    public void run() {

        int results[] = {};
        try {

            results = _store.read();

            int result = _calc.calculate(results);
            String[] mailTargets = _targets.findTargets(result);

            Arrays.stream(mailTargets).forEach( (target) -> _mailer.send(target, result));
        }
        catch (Exception e) {
            _logger.log("Damn jerks");
        }        
    }
}
